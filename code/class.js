class StoreElementCRM {
  constructor(
    id = () => {},
    dateNow = () => {},
    productName = "",
    productPrice = "",
    productImage = "/img/error.png",
    productDescription = "",
    productQuantity = 0,
    keywords = [],
    status = false
  ) {
    this.id = id();
    this.date = dateNow();
    this.productName = productName;
    this.productPrice = productPrice;
    this.productImage = productImage;
    this.productDescription = productDescription;
    this.productQuantity = productQuantity;
    this.keywords = keywords.split(",");
    this.status = false;
  }
}

export { StoreElementCRM };
