import {
  removeArrayItemEvent,
  editArrayItemEvent,
  createHTMLElement,
} from "./functions.js";

function showStoreProduct(arr = []) {
  const tbody = document.querySelector("tbody");
  tbody.innerHTML = "";
  arr.forEach(function (obj, i) {
    const tr = createHTMLElement("tr");
    const element = [
      createHTMLElement("td", undefined, i + 1),
      createHTMLElement("td", undefined, obj.productName),
      createHTMLElement("td", undefined, obj.productQuantity),
      createHTMLElement("td", undefined, obj.productPrice),
      createHTMLElement(
        "td",
        undefined,
        `<span data-key ="${obj.id}" class="icon">&#9998;</span>`,
        undefined,
        (e) => {
          editArrayItemEvent(
            e,
            localStorage.store,
            newSaveProductInfo,
            updateStoreStorage,
            showStoreProduct
          );
        }
      ),
      createHTMLElement(
        "td",
        undefined,
        obj.status
          ? `<span class = "icon green">&#10004;</span>`
          : `<span class = "icon red">&#10008;</span>`
      ),
      createHTMLElement("td", undefined, obj.date),
      createHTMLElement(
        "td",
        undefined,
        `<span  data-key="${obj.id}" class="icon">&#10006;</span>`,
        undefined,
        (e) => {
          removeArrayItemEvent(
            e,
            localStorage.store,
            updateStoreStorage,
            showStoreProduct
          );
        }
      ),
    ];
    tbody.append(tr);
    tr.append(...element);
    console.log(obj);
  });
}

function updateStoreStorage(val) {
  localStorage.store = val;
}

function newSaveProductInfo(newObj, oldObj) {
  const inputs = newObj.querySelectorAll("input");

  const obj = {
    id: oldObj.id,
    date: oldObj.date,
    status: oldObj.status,
  };
  inputs.forEach((input) => {
    switch (input.key) {
      case "productPrice":
        obj.productPrice = input.value;
        return;
      case "productDescription":
        obj.productDescription = input.value;
        return;
      case "productImage":
        obj.productImage = input.value;
        return;
      case "productName":
        obj.productName = input.value;
        return;
      case "productQuantity":
        obj.productQuantity = input.value;
    }
  });

  if (obj.productQuantity > 0) {
    obj.status = true;
  } else {
    obj.status = false;
  }
  const store = JSON.parse(localStorage.store);
  store.splice(
    store.findIndex((el) => el.id === oldObj.id),
    1,
    obj
  );
  return store;
}

if (localStorage.store) {
  showStoreProduct(JSON.parse(localStorage.store));
}
