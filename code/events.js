import { getLogin, getPassword } from "./var.js";
import {
  validate,
  createInputString,
  generationId,
  dateNow,
} from "./functions.js";
import { StoreElementCRM } from "./class.js";
import { VideoElementCRM, RestElementCRM } from "./code.js";
const isDisableBtn = {
  flagLogin: false,
  flagPassword: false,
};

function changeInputEvent(e) {
  if (
    e.target.dataset.type === "login" &&
    validate(new RegExp("^" + getLogin + "$"), e.target.value)
  ) {
    e.target.classList.remove("error");
    isDisableBtn.flagLogin = true;
  } else if (
    e.target.dataset.type === "password" &&
    validate(new RegExp("^" + getPassword + "$"), e.target.value)
  ) {
    isDisableBtn.flagPassword = true;
    e.target.classList.remove("error");
  } else {
    e.target.classList.add("error");
    if (e.target.dataset.type === "login") {
      isDisableBtn.flagLogin = false;
    } else if (e.target.dataset.type === "password") {
      isDisableBtn.flagPassword = false;
    }
  }

  if (isDisableBtn.flagLogin && isDisableBtn.flagPassword) {
    document.getElementById("disabled").disabled = false;
  } else {
    document.getElementById("disabled").disabled = true;
  }
  console.log(`${isDisableBtn.flagLogin} - ${isDisableBtn.flagPassword}`);
}

function userLoginEvent() {
  sessionStorage.isLogin = true;
  document.location = "/";
}

function showModal() {
  const modal = document.querySelector(".container-modal");
  modal.classList.remove("hide");
}

function hideModal() {
  const modal = document.querySelector(".container-modal");
  modal.classList.add("hide");
}

function changeCategoryEvent(e) {
  const modal__body = document.querySelector(".modal__body");
  modal__body.innerHTML = "";
  console.log(e.target.value);

  if (e.target.value === "Магазин") {
    modal__body.insertAdjacentHTML(
      "beforeend",
      `<form>
${createInputString("text", "Назва продукту", generationId(), "productName")}
${createInputString(
  "number",
  "Вартість продукту",
  generationId(),
  "productPrice"
)}
${createInputString("url", "Картинка продукту", generationId(), "productImage")}
${createInputString(
  "text",
  "Опис продукту",
  generationId(),
  "productDescription"
)}
${createInputString(
  "text",
  "Ключеві слова для пошуку.Розділяти комою",
  generationId(),
  "keywords"
)}
`
    );
  } else if (e.target.value === "Відео хостинг") {
    modal__body.insertAdjacentHTML(
      "beforeend",
      `<form>
${createInputString("text", "Назва відео", generationId(), "productName")}
${createInputString("url", "Постер", generationId(), "poster")}
${createInputString("url", "Посилання на відео", generationId(), "url")}
${createInputString("text", "Опис відео", generationId(), "description")}
${createInputString(
  "text",
  "Ключеві слова для пошуку.Розділяти комою",
  generationId(),
  "keywords"
)}
`
    );
  } else if (e.target.value === "Ресторан") {
    modal__body.insertAdjacentHTML(
      "beforeend",
      `<form>
${createInputString("text", "Назва страви", generationId(), "productName")}
${createInputString("text", "Грамовка", generationId(), "productWeight")}
${createInputString("text", "Склад", generationId(), "ingredients")}
${createInputString("text", "Опис продукту", generationId(), "description")}
${createInputString(
  "text",
  "Ключеві слова для пошуку.Розділяти комою",
  generationId(),
  "keywords"
)}
${createInputString("text", "Вартість продукту", generationId(), "price")}
${createInputString(
  "url",
  "Зображення продукту",
  generationId(),
  "productImageUrl"
)}
`
    );
  }
}

function saveData() {
  const isCategory = document.querySelector("select").selectedOptions[0].value;
  console.log(isCategory);
  const [...inputs] = document.querySelectorAll("form input");

  if (isCategory === "Магазин") {
    const obj = {
      productName: "",
      productPrice: "",
      productImage: "",
      productDescription: "",
      keywords: "",
    };

    inputs.forEach((e) => {
      obj[e.dataset.type] = e.value;
      e.value = "";
    });
    console.log(obj);

    const store = JSON.parse(localStorage.store);

    store.push(
      new StoreElementCRM(
        generationId,
        dateNow,
        obj.productName,
        obj.productPrice,
        obj.productImage,
        obj.productDescription,
        undefined,
        obj.keywords
      )
    );
    localStorage.store = JSON.stringify(store);

    // Мій код
  } else if (isCategory === "Відео хостинг") {
    const videoObj = {
      productName: "",
      poster: "",
      url: "",
      description: "",
      keywords: "",
    };

    inputs.forEach((e) => {
      videoObj[e.dataset.type] = e.value;
      e.value = "";
    });
    console.log(videoObj);

    const video = JSON.parse(localStorage.video);

    video.push(
      new VideoElementCRM(
        generationId,
        dateNow,
        videoObj.productName,
        videoObj.poster,
        undefined,
        videoObj.description,
        videoObj.url,
        videoObj.keywords
      )
    );
    localStorage.video = JSON.stringify(video);
  } else if (isCategory === "Ресторан") {
    const restObj = {
      productName: "",
      productWeight: "",
      ingredients: "",
      description: "",
      keywords: "",
      price: "",
      productImageUrl: "",
    };
    inputs.forEach((e) => {
      restObj[e.dataset.type] = e.value;
      e.value = "";
    });
    console.log(restObj);

    const rest = JSON.parse(localStorage.rest);

    rest.push(
      new RestElementCRM(
        generationId,
        dateNow,
        restObj.productName,
        restObj.productWeight,
        restObj.ingredients,
        restObj.description,
        restObj.keywords,
        restObj.price,
        restObj.productImageUrl
      )
    );
    localStorage.rest = JSON.stringify(rest);
  }
}

export {
  changeInputEvent,
  userLoginEvent,
  showModal,
  hideModal,
  changeCategoryEvent,
  saveData,
};
