import { modalSave, modalClose } from "./var.js";
import { showModal, hideModal, saveData } from "./events.js";
import { categorySelect } from "./functions.js";

if (
  !sessionStorage.isLogin &&
  !document.location.pathname.includes("/authorization")
) {
  document.location = "/authorization";
}

if (!localStorage.store) {
  localStorage.store = JSON.stringify([]);
}
if (!localStorage.video) {
  localStorage.video = JSON.stringify([]);
}
if (!localStorage.rest) {
  localStorage.rest = JSON.stringify([]);
}

/*main page*/

const btn = document.querySelector(".add");

btn.addEventListener("click", showModal);
const modalWindow = document.querySelector(".modal");

modalWindow.insertAdjacentHTML(
  "beforeend",
  `<h2>Додайте новий продукт до бази даних</h2>
  <div class="category"></div>
<div class="modal__body"></div>
<div class="modal__control">
</div>
`
);
document.querySelector(".modal__control").append(modalSave, modalClose);

categorySelect();

modalClose.addEventListener("click", hideModal);

modalSave.addEventListener("click", saveData);

//Додаю до Вашего localStorage,щоб було зручніше первіряти

// if (!localStorage.rest) {
//   localStorage.rest = `[
//   { "id": "4kkw59w0yvv4wqmyf", "date": "2023/2/3  23:17:47", "stopList": true, "productName": "Борщ", "productWeight": "250", "ingredients": "телятина", "description": "перші страви", "keywords": "", "price": "100грн", "productImageUrl": "https://2recepta.com/recept/borshh/borshh.jpg", "like": "", "ageRestrictions": "", "quantity": "1" },
//   { "id": "3e0vyggv6zqnpr", "date": "2023/2/4  0:29:48", "productName": "Суп", "productWeight": "250", "ingredients": "курятина,локшина", "description": "перші страви", "keywords": ["курятина", "локшина", "суп"], "price": "80грн", "productImageUrl": "https://art-lunch.ru/content/uploads/2014/02/chicken-soup-000.jpg", "like": "", "stopList": false, "ageRestrictions": "", "quantity": "0" },
//   { "id": "q33qops5px", "date": "2023/2/4  0:42:13", "productName": "Вареники", "productWeight": "250", "ingredients": "картопля", "description": "друга страва", "keywords": ["картопля", "вареники"], "price": "120грн", "productImageUrl": "https://cdn.lifehacker.ru/wp-content/uploads/2021/01/18_1610543636-e1610543733950-1280x640.jpg", "like": "", "stopList": false, "ageRestrictions": "", "quantity": "0" }
// ]`;
// }

// if (!localStorage.video) {
//   localStorage.video = `[
//   {"id":"4m$gsrm7k46vnb4vf","date":"2023/2/4  0:21:7","status":false,"productName":"Яблуко","productPrice":"20гр","productImage":"https://lifeglobe.net/media/entry/6259/1a-0.jpg","productDescription":"фрукт","productQuantity":""},
//   {"id":"yfnw9wve7g7","date":"2023/2/4  0:22:44","status":true,"productName":"Банан","productPrice":"70грн","productImage":"https://www.gastronom.ru/binfiles/images/20151029/bddcbbce.jpg","productDescription":"фрукт","productQuantity":"1"},
//   {"id":"bkvf&vmtw2gx","date":"2023/2/4  0:24:21","status":false,"productName":"Цибуля","productPrice":"30грн","productImage":"https://e0.edimdoma.ru/data/ingredients/0000/1035/1035-ed4_wide.jpg?1482770774","productDescription":"овоч","productQuantity":"0"}
// ]`;
// }

// if (!localStorage.store) {
//   localStorage.store = `[
//     {"id":"tgpk$7kemt","date":"2023/2/4  0:34:40","productName":"різні жахи","poster":"","like":"","description":"жахи","url":"https://www.youtube.com/watch?v=VvoJBJyMEAE","keywords":["жахи"]},
//     {"id":"f50gcrnwf$&0","date":"2023/2/4  0:38:13","productName":"різні комедії","poster":"","like":"","description":"комедії","url":"https://www.youtube.com/watch?v=9SOpgV9cxO4","keywords":["комедії"]},
//     {"id":"q1fpfr6o99yoqpko","date":"2023/2/4  0:40:4","productName":"фантастика","poster":"","like":"","description":"фантастика","url":"https://www.youtube.com/watch?v=WKI2PpolGXE","keywords":["фантастика"]}
//   ]`;
// }
