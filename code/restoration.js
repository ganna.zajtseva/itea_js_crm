import {
  removeArrayItemEvent,
  editArrayItemEvent,
  createHTMLElement,
} from "./functions.js";

function showRestoranMenu(arr = []) {
  const tbody = document.querySelector("tbody");
  tbody.innerHTML = "";
  arr.forEach(function (obj, i) {
    const tr = createHTMLElement("tr");
    const element = [
      createHTMLElement("td", undefined, i + 1),
      createHTMLElement("td", undefined, obj.productName),
      createHTMLElement("td", undefined, obj.quantity),
      createHTMLElement("td", undefined, obj.price),
      createHTMLElement(
        "td",
        undefined,
        `<span data-key ="${obj.id}" class="icon">&#9998;</span>`,
        undefined,
        (e) => {
          editArrayItemEvent(
            e,
            localStorage.rest,
            newSaveRestInfo,
            updateRestStorage,
            showRestoranMenu
          );
        }
      ),
      createHTMLElement(
        "td",
        undefined,
        obj.stopList
          ? `<span class="icon green">&#10004;</span>`
          : `<span class="icon red">&#10008;</span>`
      ),
      createHTMLElement("td", undefined, obj.date),
      createHTMLElement(
        "td",
        undefined,
        `<span data-key="${obj.id}" class="icon">&#10006;</span>`,
        undefined,
        (e) => {
          removeArrayItemEvent(
            e,
            localStorage.rest,
            updateRestStorage,
            showRestoranMenu
          );
        }
      ),
    ];

    tbody.append(tr);
    tr.append(...element);
    console.log(obj);
  });
}

function updateRestStorage(val) {
  localStorage.rest = val;
}

function newSaveRestInfo(newObj, oldObj) {
  const inputs = newObj.querySelectorAll("input");

  const obj = {
    id: oldObj.id,
    date: oldObj.date,
    stopList: oldObj.stopList,
  };
  inputs.forEach((input) => {
    obj[input.key] = input.value;
  });

  obj.stopList = (obj.quantity > 0);


  const rest = JSON.parse(localStorage.rest);
  rest.splice(
    rest.findIndex((el) => el.id === oldObj.id),
    1,
    obj
  );
  return rest;
}


if (localStorage.rest) {
  showRestoranMenu(JSON.parse(localStorage.rest));
}
