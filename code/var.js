import { createHTMLElement } from "./functions.js";

export const dayEn = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
];
export const dayUk = [
  "Неділя",
  "Понеділок",
  "Вівторок",
  "Середа",
  "Четвер",
  "П'ятниця",
  "Субота",
];

export const getLogin = `${
  dayEn[new Date().getDay()]
}${new Date().getHours()}`.toLowerCase();
export const getPassword = `${
  dayUk[new Date().getDay()]
}${new Date().getHours()}${new Date().getMinutes()}${new Date().getFullYear()}`.toLocaleLowerCase();
export const modalSave = createHTMLElement("button", "btn_save", "Зберегти", [
  { type: "button" },
  { datatype: "button" },
]);
export const modalClose = createHTMLElement(
  "button",
  "btn_close",
  "Скасувати",
  [{ type: "button" }, { datatype: "button" }]
);
