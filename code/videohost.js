import {
  removeArrayItemEvent,
  editArrayItemEvent,
  createHTMLElement,
} from "./functions.js";

function showVideoProduct(arr = []) {
  const tbody = document.querySelector("tbody");
  tbody.innerHTML = "";
  arr.forEach(function (obj, i) {
    const tr = createHTMLElement("tr");
    const element = [
      createHTMLElement("td", undefined, i + 1),
      createHTMLElement("td", undefined, obj.productName),
      createHTMLElement("td", undefined, obj.date),
      createHTMLElement(
        "td",
        "video",
        ` <a  href = "${obj.url}">${obj.url}</a>`
      ),
      createHTMLElement(
        "td",
        undefined,
        `<span   data-key ="${obj.id}"class = "icon">&#9998;</span>`,
        undefined,
        (e) => {
          editArrayItemEvent(
            e,
            localStorage.video,
            newSaveVideoInfo,
            updateVideoStorage,
            showVideoProduct
          );
        }
      ),

      createHTMLElement(
        "td",
        undefined,
        `<span  data-key ="${obj.id}" class = icon>&#10006;</span>`,
        undefined,
        (e) => {
          removeArrayItemEvent(
            e,
            localStorage.video,
            updateVideoStorage,
            showVideoProduct
          );
        }
      ),
    ];
    tbody.append(tr);
    tr.append(...element);
    console.log(obj);
  });
}

function updateVideoStorage(val) {
  localStorage.video = val;
}

function newSaveVideoInfo(newObj, oldObj) {
  const inputs = newObj.querySelectorAll("input");

  const obj = {
    id: oldObj.id,
    date: oldObj.date,
    stopList: oldObj.stopList,
  };
  inputs.forEach((input) => {
    obj[input.key] = input.value;
  });
  const video = JSON.parse(localStorage.video);
  video.splice(
    video.findIndex((el) => el.id === oldObj.id),
    1,
    obj
  );
  return video;
}

if (localStorage.video) {
  showVideoProduct(JSON.parse(localStorage.video));
}
