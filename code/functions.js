import { changeCategoryEvent, hideModal, showModal } from "./events.js";
import { modalClose, modalSave } from "./var.js";

export const validate = (p, v) => p.test(v);

export const generationId = () => {
  const sizeID = Math.floor(Math.random() * (10 - 18) + 18);
  const a = "qwertyopckvknvfgvbnmzxs1234567890$&";
  let r = ""; //18

  for (let i = 0; i < sizeID; i++) {
    r += a[Math.floor(Math.random() * a.length)];
  }
  return r;
};

export function createHTMLElement(
  tagName = "div",
  className,
  value,
  attr = [],
  listener = () => {}
) {
  const el = document.createElement(tagName);

  if (className) {
    el.classList.add(className);
  }
  if ("inputtrxtareaoption".includes(tagName)) {
    if (value) {
      el.value = value;
    }
    if (listener) {
      el.addEventListener("change", listener);
    }
  } else {
    if (value !== undefined) {
      el.innerHTML = value;
    }
    if (listener) {
      el.addEventListener("click", listener);
    }
  }
  if (typeof attr === "object" && attr !== null) {
    attr.forEach(
      (attr) => {
        el.setAttribute(Object.keys(attr), Object.values(attr));
      }
      //  attr.forEach((attr) => {
      //     el.setAttribute(Object.entries(attr)[0][0], Object.entries(attr)[0][1]);
      // }
    );
  }
  return el;
}

export function dateNow() {
  return `${new Date().getFullYear()}/${new Date().getMonth()}/${new Date().getDay()}  ${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}`;
}

export function categorySelect() {
  const category = document.querySelector(".category");

  category.insertAdjacentHTML(
    "beforeend",
    `<select id="category"><option value="" disabled selected >Оберіть категорію</option><option value="Магазин">Магазин</option><option value="Відео хостинг">Відео хостинг</option><option value="Ресторан">Ресторан</option></select>`
  );
  document
    .querySelector("#category")
    .addEventListener("change", changeCategoryEvent);
}

export function createInputString(type = "text", value = "", id, key) {
  const input = `<div class="element-product">
  <label for="${id}">${value}</label>
  <input type="${type}" id = "${id}" data-type = "${key}"></div>`;
  return input;
}

export function createEditProductInput(p, v) {
  const div = createHTMLElement("div", undefined);
  const id = generationId();
  const label = createHTMLElement("label", undefined, p, [{ for: id }]);
  const input = createHTMLElement("input", undefined, v);
  input.key = p;
  if (p === "status") {
    input.type = "checkbox";
  }
  if (p === "id" || p === "date" || p === "status") {
    input.disabled = true;
  }
  div.append(label, input);
  input.id = id;
  return div;
}

// Generic functions
export function removeArrayItemEvent(
  e,
  storageItem,
  updateLocalStorage,
  showArrayItems
) {
  if (!e.target.tagName === "SPAN") return;
  let result = confirm("Ви впевненні, що треба видалити ?");
  if (result == true) {
    const span = e.target;
    const store = JSON.parse(storageItem);

    store.splice(
      store.findIndex((el) => el.id === span.dataset.key),
      1
    );

    updateLocalStorage(JSON.stringify(store));
    showArrayItems(store);
  }
}

export function editArrayItemEvent(
  e,
  storageItem,
  newSaveInfo,
  updateLocalStorage,
  showArrayItems
) {
  if (!e.target.tagName === "SPAN") return;
  showModal();
  const span = e.target;
  const store = JSON.parse(storageItem);
  const modalWindow = document.querySelector(".modal");
  const modalBody = createHTMLElement("div", "modal-body");
  modalWindow.append(modalBody);
  const btns = createHTMLElement("div", "btns-save");

  modalSave.addEventListener("click", () => {
    const editedStore = newSaveInfo(modalBody, rez);
    updateLocalStorage(JSON.stringify(editedStore));
    hideModal();
    modalBody.remove();
    showArrayItems(editedStore);
  });

  modalClose.addEventListener("click", () => {
    hideModal();
    modalBody.remove();
  });
  btns.append(modalSave, modalClose);
  modalWindow.append(btns);

  const rez = store.find((a) => {
    return span.dataset.key === a.id;
  });
  const data = Object.entries(rez);
  const inputsElements = data.map(([props, value]) => {
    return createEditProductInput(props, value);
  });

  modalBody.append(...inputsElements);
}
