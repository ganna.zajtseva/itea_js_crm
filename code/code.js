1; /* Додати нові продукти (відео та страви) */


export class VideoElementCRM {
  constructor(
    id = () => {},
    dateNow = () => {},
    productName = "",
    poster = "/img/error.png",
    like = "",
    description = "",
    url = "",
    keywords = []
  ) {
    this.id = id();
    this.date = dateNow();
    this.productName = productName;
    this.poster = poster;
    this.like = like;
    this.description = description;
    this.url = url;
    this.keywords = keywords.split(",");
  }
}

export class RestElementCRM {
  constructor(
    id = () => {},
    dateNow = () => {},
    productName = "",
    productWeight = "",
    ingredients = "",
    description = "",
    keywords = [],
    price = "",
    productImageUrl = "",
    like = "",
    stopList = false,
    ageRestrictions = "",
    quantity = "0"
  ) {
    this.id = id();
    this.date = dateNow();
    this.productName = productName;
    this.productWeight = productWeight;
    this.ingredients = ingredients;
    this.description = description;
    this.keywords = keywords.split(",");
    this.price = price;
    this.productImageUrl = productImageUrl;
    this.like = like;
    this.stopList = false;
    this.ageRestrictions = ageRestrictions;
    this.quantity = quantity;
  }
}


 