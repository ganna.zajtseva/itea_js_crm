import { getLogin, getPassword } from "./var.js";
import { changeInputEvent, userLoginEvent } from "./events.js";

try {
  document
    .querySelector(".window form")
    .addEventListener("change", changeInputEvent);

  document.getElementById("disabled").addEventListener("click", userLoginEvent);
} catch (error) {
  console.error(error);
}

console.log(getLogin);
console.log(getPassword);
